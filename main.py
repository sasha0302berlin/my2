import random
import secrets
import string
import openpyxl
from PyQt6.QtWidgets import QApplication, QMainWindow, QPushButton, QLabel, QLineEdit, QVBoxLayout, QWidget, \
    QMessageBox, QFileDialog, QTabWidget, QHBoxLayout, QRadioButton
from PyQt6.QtCore import Qt
from PyQt6.QtGui import QPixmap, QIcon
import sys
from pyad import *
from pyad.pyadconstants import ADS_USER_FLAG

global domain


class ConnectWindow(QMainWindow):
    """основное окошко, где осуществляется подключение к АД"""

    def __init__(self):
        super().__init__()

        self.setWindowTitle("Active Directory Connection")
        self.setGeometry(100, 100, 400, 200)
        icon = QIcon("test.png")
        self.setWindowIcon(icon)
        widget = QWidget()
        self.setCentralWidget(widget)

        self.k732Label = QLabel("K732")
        self.domainLabel = QLabel("Domain:")
        self.domainInput = QLineEdit()
        self.usernameLabel = QLabel("Имя пользователя:")
        self.usernameInput = QLineEdit()
        self.passwordLabel = QLabel("Пароль:")
        self.passwordInput = QLineEdit()
        self.passwordInput.setEchoMode(QLineEdit.EchoMode.Password)
        self.connectButton = QPushButton("Подключение")

        layout = QVBoxLayout()
        layout.addWidget(self.k732Label)
        layout.addWidget(self.domainLabel)
        layout.addWidget(self.domainInput)
        layout.addWidget(self.usernameLabel)
        layout.addWidget(self.usernameInput)
        layout.addWidget(self.passwordLabel)
        layout.addWidget(self.passwordInput)
        layout.addWidget(self.connectButton)

        widget.setLayout(layout)

        self.connectButton.clicked.connect(self.connect_to_ad)

    def connect_to_ad(self):
        global domain
        domain = self.domainInput.text()
        username = self.usernameInput.text()
        password = self.passwordInput.text()

        try:
            pyad.set_defaults(ldap_server=domain, username=username, password=password)
            # Выполнение операций с Active Directory
            com = domain.split(".")
            testdomain = "cn=Users, dc=" + com[0] + ", dc=" + com[1]
            ou = aduser.ADUser.from_cn(username)
            # new_user = aduser.ADUser.from_dn("userman")
            self.close()
            self.admin_window = AdminWindow()
            self.admin_window.show()
        except Exception as e:
            QMessageBox.critical(self, "Error", f"Failed to connect to Active Directory: {str(e)}")


class AdminWindow(QMainWindow):
    """Окошка для работы с учётными записями домена"""

    def __init__(self):
        super().__init__()
        self.setWindowTitle("Admin Window")
        self.setGeometry(100, 100, 400, 200)
        icon = QIcon("test.png")
        self.setWindowIcon(icon)
        self.tab_widget = QTabWidget()

        self.create_users_tab = QWidget()
        self.disable_users_tab = QWidget()
        self.create_manual_user_tab = QWidget()

        self.tab_widget.addTab(self.create_users_tab, "Создание")
        self.tab_widget.addTab(self.disable_users_tab, "Отключение")
        self.tab_widget.addTab(self.create_manual_user_tab, "Создание пользователя")

        self.layout_create = QVBoxLayout()
        self.layout_disable = QVBoxLayout()
        self.layout_create_manual = QVBoxLayout()

        self.yearLabel = QLabel("Год поступления:")
        self.yearInput = QLineEdit()
        self.groupLabel = QLabel("Номер группы:")
        self.groupInput = QLineEdit()
        self.userCountLabel = QLabel("Количетсво пользователей:")
        self.userCountInput = QLineEdit()

        self.createUsersButton = QPushButton("Создать пользователя")
        self.createUsersButton.setEnabled(False)

        self.yearInput.textChanged.connect(self.check_inputs)
        self.groupInput.textChanged.connect(self.check_inputs)
        self.userCountInput.textChanged.connect(self.check_inputs)

        self.exportToExcelButton = QPushButton("Экспортировать в Exel")
        self.backToADButton = QPushButton("Назад")

        self.layout_create.addWidget(self.yearLabel)
        self.layout_create.addWidget(self.yearInput)
        self.layout_create.addWidget(self.groupLabel)
        self.layout_create.addWidget(self.groupInput)
        self.layout_create.addWidget(self.userCountLabel)
        self.layout_create.addWidget(self.userCountInput)
        self.layout_create.addWidget(self.createUsersButton)
        self.layout_create.addWidget(self.exportToExcelButton)
        self.layout_create.addWidget(self.backToADButton)

        self.disable_users_years_label = QLabel("Год поступления:")
        self.disable_users_years_input = QLineEdit()
        self.disable_users_group_label = QLabel("Группа:")
        self.disable_users_group_input = QLineEdit()
        self.disableUsersButton = QPushButton("Отключить пользователей")

        self.layout_disable.addWidget(self.disable_users_years_label)
        self.layout_disable.addWidget(self.disable_users_years_input)
        self.layout_disable.addWidget(self.disable_users_group_label)
        self.layout_disable.addWidget(self.disable_users_group_input)
        self.layout_disable.addWidget(self.disableUsersButton)
        self.layout_disable.setSpacing(0)

        self.usernameLabel = QLabel("Имя пользователя:")
        self.usernameInput = QLineEdit()
        self.passwordLabel = QLabel("Пароль")
        self.passwordInput = QLineEdit()
        self.passwordInput.setEchoMode(QLineEdit.EchoMode.Password)
        self.role_label = QLabel("Роль:")
        self.teacher_radio_button = QRadioButton("Преподаватель")
        self.user_radio_button = QRadioButton("Пользователь")

        self.create_manual_user_button = QPushButton("Create User")
        self.create_manual_user_button.setEnabled(False)
        self.usernameInput.textChanged.connect(self.check_inputs_manual)
        self.passwordInput.textChanged.connect(self.check_inputs_manual)

        self.layout_create_manual.addWidget(self.usernameLabel)
        self.layout_create_manual.addWidget(self.usernameInput)
        self.layout_create_manual.addWidget(self.passwordLabel)
        self.layout_create_manual.addWidget(self.passwordInput)
        self.layout_create_manual.addWidget(self.role_label)

        role_layout = QHBoxLayout()

        role_layout.addWidget(self.teacher_radio_button)  # Выбор учителя
        role_layout.addWidget(self.user_radio_button)  # Выбор пользователя
        self.layout_create_manual.addLayout(role_layout)

        self.layout_create_manual.addWidget(self.create_manual_user_button)

        self.createUsersButton.clicked.connect(self.create_users)
        self.disableUsersButton.clicked.connect(self.disable_users)
        self.create_manual_user_button.clicked.connect(self.create_manual_user)
        self.exportToExcelButton.clicked.connect(self.export_to_excel)
        self.backToADButton.clicked.connect(self.back_to_AD)
        self.create_users_tab.setLayout(self.layout_create)
        self.disable_users_tab.setLayout(self.layout_disable)
        self.create_manual_user_tab.setLayout(self.layout_create_manual)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.tab_widget)
        widget = QWidget()
        widget.setLayout(self.layout)
        self.setCentralWidget(widget)

        self.users_and_passwords = []

    def define_students_group_container(self):
        """
        Определение контейнера для группы студентов домена AD
        """
        global domain
        try:
            parse = domain.split(".")
            ou = "ou=Students," + "dc=" + parse[0] + ",dc=" + parse[1]
            students_container = pyad.adcontainer.ADContainer.from_dn(ou)
        except Exception as e:
            parse = domain.split(".")

            ou = "dc=" + parse[0] + ",dc=" + parse[1]
            parent_cont = adcontainer.ADContainer.from_dn(ou)
            print(parent_cont)
            students_container = pyad.adcontainer.ADContainer.create_container(
                self=parent_cont,
                name="Students",
                optional_attributes={
                    'description': 'контейнер для хранения учётных записей студентов'
                }
            )

            print(e)

        return students_container

    def define_students_years_group(self, years):
        """
        Определение группы для студентов домена AD
        @param years:год поступления студентов
        """
        global domain
        parse = domain.split(".")
        try:

            ou = "ou=" + years + ",ou=Students," + "dc=" + parse[0] + ",dc=" + parse[1]
            year_students_container = pyad.adcontainer.ADContainer.from_dn(ou)
        except Exception as e:
            ou = "ou=Students,dc=" + parse[0] + ",dc=" + parse[1]
            parent_cont = adcontainer.ADContainer.from_dn(ou)
            print(ou)
            year_students_container = pyad.adcontainer.ADContainer.create_container(
                self=parent_cont,
                name=years,
                optional_attributes={
                    'description': 'контейнер для хранения учётных записей студентов'
                }
            )

            print(e)

        return year_students_container

    def define_group_container(self, years, grops):
        """
        Определение контейнера для студентов конкретной группы
        @param years:год поступления студентов
        @param grops: конкретная группа студентов
        """

        global domain
        parse = domain.split(".")
        try:
            ou = "ou=" + grops + ",ou=" + years + ",ou=Students," + "dc=" + parse[0] + ",dc=" + parse[1]
            group_students_container = pyad.adcontainer.ADContainer.from_dn(ou)
        except Exception:
            ou = "ou=" + years + ",ou=Students,dc=" + parse[0] + ",dc=" + parse[1]
            parent_cont = adcontainer.ADContainer.from_dn(ou)
            group_students_container = pyad.adcontainer.ADContainer.create_container(
                self=parent_cont,
                name=f'{grops}',
                optional_attributes={
                    'description': f'контейнер для группы № {grops}, сформированной в {years}'
                }
            )

        return group_students_container

    def define_group(self, years, grops):
        """
        Определение конкретной группы студентов
        @param years:год поступления студентов
        @param grops: конкретная группа студентов
        """
        global domain
        parse = domain.split(".")
        try:
            ou = ("cn=" + grops + ",ou=" + grops + ",ou=" + years + ",ou=Students," + "dc=" + parse[0] +
                  ",dc=" + parse[1])
            group = pyad.adgroup.ADGroup.from_dn(ou)
        except Exception:
            ou = "ou=" + grops + ",ou=" + years + ",ou=Students,dc=" + parse[0] + ",dc=" + parse[1]
            parent_cont = adcontainer.ADContainer.from_dn(ou)
            group = pyad.adgroup.ADGroup.create(
                name=f'{grops}',
                container_object=parent_cont,
                security_enabled=True,
                scope='UNIVERSAL',
                optional_attributes={
                    "description": f"группа № {grops}, сформированная в {years}"
                }
            )

        return group

    def define_student(self, years, grops, username, password):
        """
        @param years:год поступления студентов
        @param grops: конкретная группа студентов
        @param username: имя студента
        @param password:пароль, генерируемый для студента
        """
        global domain
        parse = domain.split(".")
        # поиск уже созданного конкретного студента и актуализация его пароля
        try:
            ou = "cn=" + username + ",ou=" + grops + ",ou=" + years + ",ou=Students," + "dc=" + parse[0] + ",dc=" + \
                 parse[1]
            student = pyad.aduser.ADUser.from_dn(ou)

        # создание нового конкретной студента
        except Exception:
            ou = "ou=" + grops + ",ou=" + years + ",ou=Students,dc=" + parse[0] + ",dc=" + parse[1]
            parent_cont = adcontainer.ADContainer.from_dn(ou)
            student = pyad.adcontainer.ADContainer.create_user(
                self=parent_cont,
                name=f'{username}',
                password=f'{password}',
                optional_attributes={
                    'givenName': f'{username}',
                    'displayName': f'студент № {username[-2:]}',
                    'description': f'студент № {username[-2:]} группы № {grops}, сформированной в {years}',
                    'title': 'слушатель',
                    'company': 'институт криптографии связи и информатики'

                }
            )

        student.set_user_account_control_setting('DONT_EXPIRE_PASSWD', True)
        gu = "cn=" + grops + ",ou=" + grops + ",ou=" + years + ",ou=Students," + "dc=" + parse[0] + ",dc=" + parse[1]
        group = pyad.adgroup.ADGroup.from_dn(gu)
        # добавление студента в группу
        pyad.adgroup.ADGroup.add_to_group(self=student, group=group)
        return student

    def generate_password(self):
        """
        Генерация пароля пользователя
        @param password_length: int, - длина пароля
        @return: str, - сгенерированный пароль
        """
        lower_letters = string.ascii_lowercase
        upper_letters = string.ascii_uppercase
        digits = string.digits

        password = ''.join(secrets.choice(secrets.choice([lower_letters, upper_letters, digits]))
                           for _ in range(10))

        return password

    def create_users(self):
        global domain
        parse = domain.split(".")
        year = self.yearInput.text()
        group_number = self.groupInput.text()
        user_count = int(self.userCountInput.text())

        try:
            self.define_students_group_container()
        except Exception as e:
            QMessageBox.critical(self, "Error", f"Failed to create group Stud : {str(e)}")
        try:
            self.define_students_years_group(year)
        except Exception as e:
            QMessageBox.critical(self, "Error", f"Failed to create group  : {str(e)}")
        try:
            self.define_group_container(year, group_number)
        except Exception as e:
            QMessageBox.critical(self, "Error", f"Failed to create group number : {str(e)}")
        try:
            self.define_group(year, group_number)
        except Exception as e:
            QMessageBox.critical(self, "Error", f"Failed to create group number  2: {str(e)}")
        self.users_and_passwords = []
        for i in range(user_count):
            username = year + group_number + '{:02d}'.format(i + 1)

            password = self.generate_password()
            self.users_and_passwords.append((username, password))
            try:
                i = 1
                while i < int(user_count):
                    new_user = self.define_student(year, group_number, username, password)
                    i += 1


            except Exception as e:
                QMessageBox.critical(self, "Error", f"Failed to create user {username}: {str(e)}")
            print(f"User {username} successfully created")

    def export_to_excel(self):
        year = self.yearInput.text()
        group_number = self.groupInput.text()
        file_path, _ = QFileDialog.getSaveFileName(self, "Save File",
                                                   "", "Excel Files (*.xlsx);;All Files (*)")

        if file_path:
            try:
                # Пытаемся загрузить существующий файл
                workbook = openpyxl.load_workbook(file_path)
                print(f"Файл '{file_path}' найден. Добавляем новый лист.")
            except FileNotFoundError:
                # Если файл не найден, создаем новый
                workbook = openpyxl.Workbook()
                print(f"Файл '{file_path}' не найден. Создаем новый файл.")
            sheet = workbook.create_sheet()
            sheet.title = "группа" + " 20" + year + "-" + group_number
            sheet.cell(row=1, column=1, value="Username")
            sheet.cell(row=1, column=2, value="Password")

            row = 2
            for user, password in self.users_and_passwords:
                sheet.cell(row=row, column=1, value="S" + user)
                sheet.cell(row=row, column=2, value=password)
                row += 1

            workbook.save(file_path)
        self.users_and_passwords = []

    def disable_users(self):
        group = self.disable_users_group_input.text()
        year = self.disable_users_years_input.text()
        global domain
        parse = domain.split(".")
        print(ADS_USER_FLAG['LOCKOUT'])
        try:

            ou = "ou=" + group + ",ou=" + year + ",ou=Students,dc=" + parse[0] + ",dc=" + parse[1]
            group_year_students_container = pyad.adcontainer.ADContainer.from_dn(ou)

            children = pyad.adcontainer.ADContainer.get_children(self=group_year_students_container)

            i = 0
            while i < len(children) -1:

                username = year + group + '{:02d}'.format(i + 1)
                print(username)
                ou = "cn=" + username + ",ou=" + group + ",ou=" + year + ",ou=Students," + "dc=" + parse[0] + ",dc=" + \
                     parse[1]
                try:
                    student = pyad.aduser.ADUser.from_dn(ou)
                    student.set_user_account_control_setting(userFlag='ACCOUNTDISABLE', newValue=True)
                    i += 1
                except Exception:
                    i += 1

        except Exception as e:
            QMessageBox.critical(self, "Error", f"Failed to delete : {str(e)}")

    def create_manual_user(self):
        username = self.usernameInput.text()
        password = self.passwordInput.text()
        role = "Teacher" if self.teacher_radio_button.isChecked() else "Users"
        global domain
        parse = domain.split(".")
        # поиск уже созданного конкретного студента и актуализация его пароля
        try:
            ou = "cn=" + username + ",cn=Users," + "dc=" + parse[0] + ",dc=" + \
                 parse[1]
            manual_user = pyad.aduser.ADUser.from_dn(ou)

        # создание нового конкретной студента
        except Exception:
            ou = "cn=Users," + "dc=" + parse[0] + ",dc=" + parse[1]
            parent_cont = adcontainer.ADContainer.from_dn(ou)
            student = pyad.adcontainer.ADContainer.create_user(
                self=parent_cont,
                name=f'{username}',
                password=f'{password}',
                optional_attributes={
                    'givenName': f'{username}',
                    'displayName': f'пользователь:{role}',
                    'description': f'пользователь:{role}',
                    'title': f'{role}',
                    'company': 'институт криптографии связи и информатики'

                }
            )
        print(f"Creating manual user - Username: {username}, Role: {role}")

    def check_inputs_manual(self):
        username = self.usernameInput.text()
        password = self.passwordInput.text()
        if username and password:
            self.create_manual_user_button.setEnabled(True)
        else:
            self.create_manual_user_button.setEnabled(False)

    def check_inputs(self):
        year = self.yearInput.text()
        group = self.groupInput.text()
        users = self.userCountInput.text()
        if year and group and users:
            self.createUsersButton.setEnabled(True)
        else:
            self.createUsersButton.setEnabled(False)

    def back_to_AD(self):
        self.close()
        window.show()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = ConnectWindow()
    window.show()
    sys.exit(app.exec())
